# https://www.codewars.com/kata/52f831fa9d332c6591000511

import re
from collections import defaultdict

def parse_molecule(formula):
    lmb = lambda n: 1 if n == '' else int(n)
    patern1 = r'^[\(\[\{]|[\)\]\}]$'
    patern2 = r'([A-Z][a-z]|[A-Z]|[\(\[\{]\w+[\)\]\}]|[\{].+[\}]|[\(\[\{].+[\)\]\}])(\d+)?'
    def elemcounter(elem):
        subLen = len(elem)
        for i in range(subLen):
            element, count = elem.pop(0)
            elem += [[re.sub(patern1, '', z[0]), lmb(z[1]) * lmb(count)] for z in re.findall(patern2, element)]
            if i+1 == subLen and len(elem) == subLen:
                return elem
            elif len(elem) == subLen:
                continue
            else:
                return elemcounter(elem)
    final = defaultdict(int)
    for k, v in elemcounter([[formula, '1']]):
        final[k] += v
    return dict(final)