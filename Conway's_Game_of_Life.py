# https://www.codewars.com/kata/52423db9add6f6fc39000354

import copy

def get_generation(cells, generations):
    newCells = copy.deepcopy(cells)

    for fff in range(generations):
        for cx in range(len(newCells)):
            newCells[cx] = [0] + newCells[cx] + [0]
        newCells = [[]] + newCells + [[]]
        for xz in range(len(newCells[1])):
            newCells[0].append(0)
            newCells[len(newCells) - 1].append(0)

        maxX = len(newCells[0]) - 1
        maxY = len(newCells) - 1

        dic = {}

        for i in range(len(newCells)):
            for z in range(len(newCells[i])):
                for f in [-1, 0, 1]:
                    for r in [-1, 0, 1]:
                        if f == 0 and r == 0:
                            continue
                        elif i + f < 0 or z + r < 0 or i + f > maxY or z + r > maxX:
                            continue
                        else:
                            try:
                                dic[(i, z)] += newCells[i+f][z+r]
                            except:
                                dic[(i, z)] = newCells[i+f][z+r]

        for i in dic:
            if dic[i] < 2 or dic[i] > 3:
                newCells[i[0]][i[1]] = 0
            if newCells[i[0]][i[1]] == 1 and (dic[i] == 2 or dic[i] == 3):
                newCells[i[0]][i[1]] = 1
            if newCells[i[0]][i[1]] == 0 and dic[i] == 3:
                newCells[i[0]][i[1]] = 1

    while True:
        gf = 0
        for df in newCells[0]:
            gf += df
        if gf == 0:
            newCells.pop(0)
        else:
            break
    while True:
        gf = 0
        for df in newCells[len(newCells) - 1]:
            gf += df
        if gf == 0:
            newCells.pop()
        else:
            break
    while True:
        gf = 0
        for df in newCells:
            gf += df[0]
        if gf == 0:
            for df in newCells:
                df.pop(0)
        else:
            break
    while True:
        gf = 0
        for df in newCells:
            gf += df[len(df) - 1]
        if gf == 0:
            for df in newCells:
                df.pop()
        else:
            break

    return newCells