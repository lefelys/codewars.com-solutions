
# Codewars solutions

Python Solutions on Codewars:


* 4 By 4 Skyscrapers
[kata](https://www.codewars.com/kata/5671d975d81d6c1c87000022) | [solution](/4_By_4_Skyscrapers.py)

* Molecule to atoms
[kata](https://www.codewars.com/kata/52f831fa9d332c6591000511) | [solution](/Molecule_to_atoms.py)

* Conway's Game of Life - Unlimited Edition
[kata](https://www.codewars.com/kata/52423db9add6f6fc39000354) | [solution](/Conway's_Game_of_Life.py)