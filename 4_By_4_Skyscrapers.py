# https://www.codewars.com/kata/5671d975d81d6c1c87000022

from itertools import permutations
from collections import defaultdict

class SkyCrap:
    def __init__(self, size):
        self.matrix = [[0 for i in range(size)] for z in range(size)]
        self.viewsDict = self.foolViewsDict(size)
    def foolViewsDict(self, size):
        arraysAll, viewsDict = [list(x) for x in permutations(range(1, size + 1))], defaultdict(list)
        for array in arraysAll:
            array, lViews, rViews = [0] + array + [0], 0, 0
            for i in range(1, len(array)-1):
                lViews += 1 if array[i] > max(array[:i]) else 0
                rViews += 1 if array[-i-1] > max(array[:-i-1:-1]) else 0
            viewsDict[(lViews, rViews)].append(array[1:-1])
        return dict(viewsDict)
    def get(self, rowOrColumn, number):
        if rowOrColumn == True:
            return self.matrix[number]
        else:
            return [i[number] for i in self.matrix]
    def set(self, rowOrColumn, number, array):
        if rowOrColumn == True:
            self.matrix[number] = array
        else:
            for c in range(len(array)):
                self.matrix[c][number] = array[c]

def solve_puzzle (clues):
    def findSolution(skyCrap, numberOf, rowORcolumn):
        startPoint = skyCrap.get(rowORcolumn, numberOf)
        combinations, check = [], False
        if cluesEdited[rowORcolumn][numberOf][0]*cluesEdited[rowORcolumn][numberOf][1]:
            combinations = skyCrap.viewsDict[tuple(cluesEdited[rowORcolumn][numberOf])]
        elif not cluesEdited[rowORcolumn][numberOf][0] and not cluesEdited[rowORcolumn][numberOf][1]:
            combinations += [x for key in skyCrap.viewsDict.keys() for x in skyCrap.viewsDict[tuple(key)]]
        else:
            for key in skyCrap.viewsDict.keys():
                if cluesEdited[rowORcolumn][numberOf][0] == key[0] or cluesEdited[rowORcolumn][numberOf][1] == key[1]:
                    combinations += skyCrap.viewsDict[tuple(key)]
        for combination in combinations:
            if check:
                return [skyCrap, True]
            if [combination[i] for i in range(len(combination)) if startPoint[i]] == [startPoint[i] for i in range(len(startPoint)) if startPoint[i]]:
                skyCrap.set(rowORcolumn, numberOf, combination)
            else:
                continue
            if not rowORcolumn and numberOf == len(skyCrap.matrix[0]) - 1:
                return [skyCrap, True]
            sub, check = findSolution(skyCrap, numberOf + (not rowORcolumn), not rowORcolumn)
            if check:
                skyCrap = sub
                return [skyCrap, True]
            else:
                skyCrap.set(rowORcolumn, numberOf, startPoint)
                continue
        return [skyCrap, True] if check else [False, False]
    sky4 = SkyCrap(4)
    cluesEdited ={
            False: [[clues[i], clues[int(len(clues)*3/4-i-1)]] for i in range(int(len(clues)/4))], #columnsClues
            True: [[clues[int(len(clues)-i-1)], clues[int(len(clues)/4+i)]] for i in range(int(len(clues)/4))] #rowsClues
            }
    return tuple(tuple(tuple(x) for x in findSolution(sky4, 0, True)[0].matrix))